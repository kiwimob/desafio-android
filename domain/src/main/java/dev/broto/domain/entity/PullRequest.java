package dev.broto.domain.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by broto on 11/19/15.
 */
public class PullRequest implements Parcelable {

    @SerializedName("html_url")
    private String url;
    private String state;
    private String title;
    private User user;
    private String body;

    @SerializedName("created_at")
    private Date createdAt;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.url);
        dest.writeString(this.state);
        dest.writeString(this.title);
        dest.writeParcelable(this.user, flags);
        dest.writeString(this.body);
        dest.writeLong(createdAt != null ? createdAt.getTime() : -1);
    }

    public PullRequest() {
    }

    protected PullRequest(Parcel in) {
        this.url = in.readString();
        this.state = in.readString();
        this.title = in.readString();
        this.user = in.readParcelable(User.class.getClassLoader());
        this.body = in.readString();
        long tmpCreatedAt = in.readLong();
        this.createdAt = tmpCreatedAt == -1 ? null : new Date(tmpCreatedAt);
    }

    public static final Parcelable.Creator<PullRequest> CREATOR = new Parcelable.Creator<PullRequest>() {
        public PullRequest createFromParcel(Parcel source) {
            return new PullRequest(source);
        }

        public PullRequest[] newArray(int size) {
            return new PullRequest[size];
        }
    };
}
