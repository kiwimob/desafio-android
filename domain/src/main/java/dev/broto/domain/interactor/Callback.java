package dev.broto.domain.interactor;

/**
 * Created by broto on 10/28/15.
 */
public interface Callback<R> {
    void onSuccess(R result);
    void onError(String error);
}