package dev.broto.domain.interactor;

import java.util.List;

import dev.broto.domain.entity.PullRequest;

/**
 * Created by broto on 11/19/15.
 */
public interface GetPullRequestsUseCase {

    void getPullRequest(String author, String repo, Callback<List<PullRequest>> callback);
}
