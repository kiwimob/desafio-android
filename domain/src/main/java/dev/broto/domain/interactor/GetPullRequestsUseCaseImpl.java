package dev.broto.domain.interactor;

import java.util.List;

import dev.broto.domain.entity.PullRequest;
import dev.broto.domain.net.GithubAPI;
import retrofit.Call;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by broto on 11/19/15.
 */
public class GetPullRequestsUseCaseImpl implements GetPullRequestsUseCase{

    private GithubAPI githubAPI;

    public GetPullRequestsUseCaseImpl(GithubAPI githubAPI) {
        this.githubAPI = githubAPI;
    }

    @Override
    public void getPullRequest(String author, String repo, final Callback<List<PullRequest>> callback) {
        Call<List<PullRequest>> call = githubAPI.getPullRequests(author, repo);
        call.enqueue(new retrofit.Callback<List<PullRequest>>() {
            @Override
            public void onResponse(Response<List<PullRequest>> response, Retrofit retrofit) {
                callback.onSuccess(response.body());
            }

            @Override
            public void onFailure(Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }
}
