package dev.broto.domain.interactor;

import java.util.List;

import dev.broto.domain.entity.Repository;

/**
 * Created by broto on 11/19/15.
 */
public interface GetRepositoriesUseCase {

    void getRepositories(int page, Callback<List<Repository>> callback);

}
