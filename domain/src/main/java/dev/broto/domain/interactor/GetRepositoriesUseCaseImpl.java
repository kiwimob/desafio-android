package dev.broto.domain.interactor;

import java.util.List;

import dev.broto.domain.entity.Repository;
import dev.broto.domain.net.GithubAPI;
import dev.broto.domain.net.RepositoryResponse;
import retrofit.Call;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by broto on 11/19/15.
 */
public class GetRepositoriesUseCaseImpl implements GetRepositoriesUseCase {

    private GithubAPI service;

    public GetRepositoriesUseCaseImpl(GithubAPI service) {
        this.service = service;
    }

    @Override
    public void getRepositories(int page, final Callback<List<Repository>> callback) {
        Call<RepositoryResponse> call = service.getRepositories(page);
        call.enqueue(new retrofit.Callback<RepositoryResponse>() {
            @Override
            public void onResponse(Response<RepositoryResponse> response, Retrofit retrofit) {
                if (response == null || response.body() == null || response.body().getRepositories() == null) {
                    callback.onError("No data available.");
                    return;
                }
                callback.onSuccess(response.body().getRepositories());
            }

            @Override
            public void onFailure(Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }
}
