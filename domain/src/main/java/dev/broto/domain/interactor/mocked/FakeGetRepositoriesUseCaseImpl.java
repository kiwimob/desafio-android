package dev.broto.domain.interactor.mocked;

import android.content.Context;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import dev.broto.domain.entity.Repository;
import dev.broto.domain.interactor.Callback;
import dev.broto.domain.interactor.GetRepositoriesUseCase;
import dev.broto.domain.net.RepositoryResponse;

/**
 * Created by broto on 11/19/15.
 */
public class FakeGetRepositoriesUseCaseImpl implements GetRepositoriesUseCase {

    private Context context;

    public FakeGetRepositoriesUseCaseImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getRepositories(int page, Callback<List<Repository>> callback) {
        StringBuilder returnString = new StringBuilder();
        InputStream fIn = null;
        InputStreamReader isr = null;
        BufferedReader input = null;
        try {
            fIn = context.getResources().getAssets()
                    .open("Repositories.json", Context.MODE_WORLD_READABLE);
            isr = new InputStreamReader(fIn);
            input = new BufferedReader(isr);
            String line = "";
            while ((line = input.readLine()) != null) {
                returnString.append(line);
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (isr != null)
                    isr.close();
                if (fIn != null)
                    fIn.close();
                if (input != null)
                    input.close();
            } catch (Exception e2) {
                e2.getMessage();
            }
        }

        String json = returnString.toString();
        Gson gson = new Gson();
        RepositoryResponse response = gson.fromJson(json, RepositoryResponse.class);

        callback.onSuccess(response.getRepositories());
    }
}
