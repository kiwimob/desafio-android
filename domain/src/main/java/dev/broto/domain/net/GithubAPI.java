package dev.broto.domain.net;

import java.util.List;

import dev.broto.domain.entity.PullRequest;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by broto on 11/19/15.
 */
public interface GithubAPI {

    @GET("/search/repositories?q=language:C#&sort=stars")
    Call<RepositoryResponse> getRepositories(@Query("page") int page);

    @GET("/repos/{creator}/{repo}/pulls")
    Call<List<PullRequest>> getPullRequests(@Path("creator") String creator, @Path("repo") String repo);

}
