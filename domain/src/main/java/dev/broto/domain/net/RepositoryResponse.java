package dev.broto.domain.net;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import dev.broto.domain.entity.Repository;

/**
 * Created by broto on 11/19/15.
 */
public class RepositoryResponse {

    @SerializedName("items")
    private List<Repository> repositories;

    public List<Repository> getRepositories() {
        return repositories;
    }

    public void setRepositories(List<Repository> repositories) {
        this.repositories = repositories;
    }
}
