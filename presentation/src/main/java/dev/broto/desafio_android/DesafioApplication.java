package dev.broto.desafio_android;

import android.app.Application;

import dev.broto.desafio_android.di.component.ApplicationComponent;
import dev.broto.desafio_android.di.component.DaggerApplicationComponent;
import dev.broto.desafio_android.di.module.AndroidModule;

/**
 * Created by broto on 11/19/15.
 */
public class DesafioApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = DaggerApplicationComponent.builder()
                .androidModule(new AndroidModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
