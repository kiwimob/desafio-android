package dev.broto.desafio_android.di.component;

import android.app.Activity;

import dagger.Component;
import dev.broto.desafio_android.di.module.ActivityModule;
import dev.broto.desafio_android.di.scope.PerActivity;

/**
 * Created by broto on 11/19/15.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    Activity activity();
}
