package dev.broto.desafio_android.di.component;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import dev.broto.desafio_android.di.module.AndroidModule;
import dev.broto.desafio_android.ui.activity.BaseActivity;

/**
 * Created by broto on 11/19/15.
 */

@Singleton
@Component(modules = AndroidModule.class)
public interface ApplicationComponent {

    void inject(BaseActivity baseActivity);

    Context context();
}
