package dev.broto.desafio_android.di.component;

import dagger.Component;
import dev.broto.desafio_android.di.module.ActivityModule;
import dev.broto.desafio_android.di.module.PullRequestModule;
import dev.broto.desafio_android.di.scope.PerActivity;
import dev.broto.desafio_android.ui.activity.PullRequestActivity;

/**
 * Created by broto on 11/19/15.
 */
@PerActivity
@Component(
        dependencies = ApplicationComponent.class,
        modules = {
                ActivityModule.class,
                PullRequestModule.class
        }
)
public interface PullRequestComponent extends ActivityComponent {
    void inject(PullRequestActivity repositoriesActivity);
}
