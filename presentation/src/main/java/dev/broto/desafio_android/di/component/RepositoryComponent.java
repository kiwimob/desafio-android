package dev.broto.desafio_android.di.component;

import dagger.Component;
import dev.broto.desafio_android.di.module.ActivityModule;
import dev.broto.desafio_android.di.module.RepositoryModule;
import dev.broto.desafio_android.di.scope.PerActivity;
import dev.broto.desafio_android.ui.activity.RepositoriesActivity;

/**
 * Created by broto on 11/19/15.
 */
@PerActivity
@Component(
        dependencies = ApplicationComponent.class,
        modules = {
                ActivityModule.class,
                RepositoryModule.class
        }
)
public interface RepositoryComponent extends ActivityComponent {
    void inject(RepositoriesActivity repositoriesActivity);
}
