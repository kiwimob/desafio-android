package dev.broto.desafio_android.di.module;

import android.app.Activity;

import dagger.Module;
import dagger.Provides;
import dev.broto.desafio_android.di.scope.PerActivity;

/**
 * Created by broto on 11/19/15.
 */

@Module
public class ActivityModule {

    private final Activity activity;

    public ActivityModule(Activity activity) {
        this.activity = activity;
    }

    @Provides
    @PerActivity
    Activity activity() {
        return this.activity;
    }
}
