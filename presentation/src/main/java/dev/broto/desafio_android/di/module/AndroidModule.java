package dev.broto.desafio_android.di.module;

import android.app.Application;
import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * Created by broto on 11/19/15.
 */

@Module
public class AndroidModule {

    private final Application application;

    public AndroidModule(Application application) {
        this.application = application;
    }

    @Provides
    public Context provideContext() {
        return application;
    }
}
