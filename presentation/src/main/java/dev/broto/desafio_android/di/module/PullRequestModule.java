package dev.broto.desafio_android.di.module;

import android.content.Context;

import com.squareup.okhttp.OkHttpClient;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import dev.broto.desafio_android.di.scope.PerActivity;
import dev.broto.desafio_android.presenter.PullRequestPresenter;
import dev.broto.desafio_android.presenter.PullRequestPresenterImpl;
import dev.broto.desafio_android.ui.activity.PullRequestActivity;
import dev.broto.desafio_android.view.PullRequestView;
import dev.broto.domain.entity.Repository;
import dev.broto.domain.interactor.GetPullRequestsUseCase;
import dev.broto.domain.interactor.GetPullRequestsUseCaseImpl;
import dev.broto.domain.interactor.mocked.FakeGetPullRequestsUseCaseImpl;
import dev.broto.domain.net.GithubAPI;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by broto on 11/19/15.
 */
@Module
public class PullRequestModule {

    private PullRequestActivity activity;
    private Repository repository;

    public PullRequestModule(PullRequestActivity activity, Repository repository) {
        this.activity = activity;
        this.repository = repository;
    }

    @PerActivity
    @Provides
    public PullRequestPresenter<PullRequestView> providePullRequestPresenter(@Named("RestAPI") GetPullRequestsUseCase useCase) {
        return new PullRequestPresenterImpl(repository, useCase);
    }

    @PerActivity
    @Provides
    @Named("RestAPI")
    public GetPullRequestsUseCase provideGetPullRequestsUseCase(GithubAPI githubAPI) {
        return new GetPullRequestsUseCaseImpl(githubAPI);
    }

    @PerActivity
    @Provides
    @Named("FakeAPI")
    public GetPullRequestsUseCase provideFakePullRequestsyUseCase(Context context) {
        return new FakeGetPullRequestsUseCaseImpl(context);
    }

    @PerActivity
    @Provides
    public GithubAPI provideGithubAPI() {
        OkHttpClient httpClient = new OkHttpClient();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.github.com")
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build();

        return retrofit.create(GithubAPI.class);
    }
}
