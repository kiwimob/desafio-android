package dev.broto.desafio_android.di.module;

import android.content.Context;

import com.squareup.okhttp.OkHttpClient;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import dev.broto.desafio_android.di.scope.PerActivity;
import dev.broto.desafio_android.presenter.RepositoryPresenter;
import dev.broto.desafio_android.presenter.RepositoryPresenterImpl;
import dev.broto.desafio_android.ui.activity.RepositoriesActivity;
import dev.broto.desafio_android.view.RepositoryView;
import dev.broto.domain.interactor.GetRepositoriesUseCase;
import dev.broto.domain.interactor.GetRepositoriesUseCaseImpl;
import dev.broto.domain.interactor.mocked.FakeGetRepositoriesUseCaseImpl;
import dev.broto.domain.net.GithubAPI;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by broto on 11/19/15.
 */
@Module
public class RepositoryModule {

    private RepositoriesActivity activity;

    public RepositoryModule(RepositoriesActivity activity) {
        this.activity = activity;
    }

    @PerActivity
    @Provides
    public RepositoryPresenter<RepositoryView> provideRepositoryPresenter(@Named("RestAPI") GetRepositoriesUseCase getRepositoriesUseCase) {
        return new RepositoryPresenterImpl(getRepositoriesUseCase);
    }

    @PerActivity
    @Provides
    @Named("RestAPI")
    public GetRepositoriesUseCase provideRepositoryUseCase(GithubAPI githubAPI) {
        return new GetRepositoriesUseCaseImpl(githubAPI);
    }

    @PerActivity
    @Provides
    @Named("FakeAPI")
    public GetRepositoriesUseCase provideFakeRepositoryUseCase(Context context) {
        return new FakeGetRepositoriesUseCaseImpl(context);
    }

    @PerActivity
    @Provides
    public GithubAPI provideGithubAPI() {
        OkHttpClient httpClient = new OkHttpClient();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.github.com")
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build();

        return retrofit.create(GithubAPI.class);
    }
}
