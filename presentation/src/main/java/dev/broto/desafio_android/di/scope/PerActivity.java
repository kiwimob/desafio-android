package dev.broto.desafio_android.di.scope;

import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by broto on 11/19/15.
 */
@Scope
@Retention(RUNTIME)
public @interface PerActivity {
}