package dev.broto.desafio_android.presenter;

/**
 * Created by broto on 11/19/15.
 */
public interface Presenter<V> {

    void create();

    void setView(V v);

}
