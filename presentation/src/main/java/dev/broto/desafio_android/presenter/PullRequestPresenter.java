package dev.broto.desafio_android.presenter;

import dev.broto.domain.entity.PullRequest;

/**
 * Created by broto on 11/19/15.
 */
public interface PullRequestPresenter<V> extends Presenter<V> {

    void onPullRequestClick(PullRequest pullRequest);
}
