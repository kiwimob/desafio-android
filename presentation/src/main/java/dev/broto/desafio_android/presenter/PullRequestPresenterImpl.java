package dev.broto.desafio_android.presenter;

import android.content.Intent;
import android.net.Uri;

import java.util.List;

import dev.broto.desafio_android.view.PullRequestView;
import dev.broto.domain.entity.PullRequest;
import dev.broto.domain.entity.Repository;
import dev.broto.domain.interactor.Callback;
import dev.broto.domain.interactor.GetPullRequestsUseCase;

/**
 * Created by broto on 11/19/15.
 */
public class PullRequestPresenterImpl implements PullRequestPresenter<PullRequestView> {

    private PullRequestView view;
    private Repository repository;
    private GetPullRequestsUseCase useCase;

    public PullRequestPresenterImpl(Repository repository, GetPullRequestsUseCase useCase) {
        this.repository = repository;
        this.useCase = useCase;
    }

    @Override
    public void create() {
        useCase.getPullRequest(
                repository.getOwner().getName(),
                repository.getName(),
                new Callback<List<PullRequest>>() {
                    @Override
                    public void onSuccess(List<PullRequest> result) {
                        view.renderPullRequests(result);
                    }

                    @Override
                    public void onError(String error) {
                        view.showErrorMessage(error);
                    }
                });
    }

    @Override
    public void setView(PullRequestView view) {
        this.view = view;
    }

    @Override
    public void onPullRequestClick(PullRequest pullRequest) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(pullRequest.getUrl()));

        view.launchActivity(i);
    }
}
