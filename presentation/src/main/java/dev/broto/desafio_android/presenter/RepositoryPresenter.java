package dev.broto.desafio_android.presenter;

/**
 * Created by broto on 11/19/15.
 */
public interface RepositoryPresenter<V> extends Presenter<V>{

    void onLoadMore();

    void onLoad();

}
