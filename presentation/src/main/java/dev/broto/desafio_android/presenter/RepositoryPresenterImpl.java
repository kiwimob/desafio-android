package dev.broto.desafio_android.presenter;

import java.util.List;

import dev.broto.desafio_android.view.RepositoryView;
import dev.broto.domain.entity.Repository;
import dev.broto.domain.interactor.Callback;
import dev.broto.domain.interactor.GetRepositoriesUseCase;

/**
 * Created by broto on 11/19/15.
 */
public class RepositoryPresenterImpl implements RepositoryPresenter<RepositoryView> {

    private int page = 1;
    private RepositoryView view;
    private GetRepositoriesUseCase useCase;

    public RepositoryPresenterImpl(GetRepositoriesUseCase useCase) {
        this.useCase = useCase;
    }

    @Override
    public void setView(RepositoryView repositoryView) {
        this.view = repositoryView;
    }

    @Override
    public void create() {
        onLoad();
    }

    @Override
    public void onLoad() {
        page = 1;
        view.showLoading();
        refresh();
    }

    @Override
    public void onLoadMore() {
        page++;
        view.showLoading();
        refresh();
    }

    private void refresh() {
        useCase.getRepositories(page, new Callback<List<Repository>>() {
            @Override
            public void onSuccess(List<Repository> result) {
                view.renderRepositories(result);
                view.hideLoading();
            }

            @Override
            public void onError(String error) {
                view.hideLoading();
                view.showErrorMessage(error);
            }
        });
    }
}
