package dev.broto.desafio_android.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import dev.broto.desafio_android.DesafioApplication;
import dev.broto.desafio_android.di.component.ApplicationComponent;
import dev.broto.desafio_android.di.module.ActivityModule;

/**
 * Created by broto on 11/19/15.
 */
public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getApplicationComponent().inject(this);
    }

    protected ApplicationComponent getApplicationComponent() {
        return ((DesafioApplication) getApplication()).getApplicationComponent();
    }

    protected ActivityModule getActivityModule() {
        return new ActivityModule(this);
    }
}
