package dev.broto.desafio_android.ui.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import dev.broto.desafio_android.R;
import dev.broto.desafio_android.di.component.DaggerPullRequestComponent;
import dev.broto.desafio_android.di.component.PullRequestComponent;
import dev.broto.desafio_android.di.module.PullRequestModule;
import dev.broto.desafio_android.presenter.PullRequestPresenter;
import dev.broto.desafio_android.ui.adapter.PullRequestViewHolder;
import dev.broto.desafio_android.view.PullRequestView;
import dev.broto.domain.entity.PullRequest;
import dev.broto.domain.entity.Repository;
import uk.co.ribot.easyadapter.EasyRecyclerAdapter;

public class PullRequestActivity extends BaseActivity implements PullRequestView, PullRequestViewHolder.PullRequestHolderListener {

    private static final String ARG_REPOSITORY = "arg_repository";

    private Repository repository;
    private ProgressDialog progress;

    @Bind(R.id.toolbar)
    public Toolbar toolbar;

    @Bind(R.id.recycler_view)
    public RecyclerView recyclerView;

    @Inject
    public PullRequestPresenter<PullRequestView> presenter;

    public static Intent newInstance(Context context, Repository repository) {
        if (repository == null)
            throw new NullPointerException();

        Intent intent = new Intent(context, PullRequestActivity.class);
        intent.putExtra(ARG_REPOSITORY, repository);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_request);

        ButterKnife.bind(this);

        repository = getIntent().getParcelableExtra(ARG_REPOSITORY);

        initInjector();
        initToolbar();
        initPresenter();
    }

    @Override
    public void launchActivity(Intent intent) {
        startActivity(intent);
    }

    @Override
    public void renderPullRequests(List<PullRequest> pullRequests) {
        if (pullRequests == null || pullRequests.size() <= 0)
            return;

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(new EasyRecyclerAdapter<>(
                this,
                PullRequestViewHolder.class,
                pullRequests,
                this));
    }

    @Override
    public void showLoading() {
        if (progress == null || !progress.isShowing()) {
            progress = ProgressDialog.show(this, getString(R.string.wait),
                    getString(R.string.loading), true);
        }
    }

    @Override
    public void hideLoading() {
        if (progress != null && progress.isShowing()) {
            progress.dismiss();
            progress = null;
        }
    }

    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(PullRequestActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPullRequestClicked(PullRequest pullRequest) {
        presenter.onPullRequestClick(pullRequest);
    }

    private void initInjector() {
        PullRequestComponent component = DaggerPullRequestComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(getActivityModule())
                .pullRequestModule(new PullRequestModule(this, repository))
                .build();
        component.inject(this);
    }

    private void initToolbar() {
        toolbar.setTitle(getString(R.string.pull_requests));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initPresenter() {
        presenter.setView(this);
        presenter.create();
    }
}
