package dev.broto.desafio_android.ui.activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import dev.broto.desafio_android.R;
import dev.broto.desafio_android.di.component.DaggerRepositoryComponent;
import dev.broto.desafio_android.di.component.RepositoryComponent;
import dev.broto.desafio_android.di.module.RepositoryModule;
import dev.broto.desafio_android.presenter.RepositoryPresenter;
import dev.broto.desafio_android.ui.adapter.RepositoryViewHolder;
import dev.broto.desafio_android.view.RepositoryView;
import dev.broto.domain.entity.Repository;
import uk.co.ribot.easyadapter.EasyRecyclerAdapter;

public class RepositoriesActivity extends BaseActivity implements RepositoryView, RepositoryViewHolder.RepositoryHolderListener {

    @Bind(R.id.toolbar)
    public Toolbar toolbar;

    @Bind(R.id.recycler_view)
    public UltimateRecyclerView recyclerView;

    @Bind(R.id.activity_main_swipe_refresh_layout)
    public SwipeRefreshLayout mSwipeRefreshLayout;

    @Inject
    public RepositoryPresenter<RepositoryView> presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repository);

        ButterKnife.bind(this);

        initInjector();
        initRecyclerView();
        initSwipeRefresh();
        initToolbar();
        initPresenter();
    }

    @Override
    public void showLoading() {
        mSwipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideLoading() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void renderRepositories(List<Repository> repositoryModelList) {
        mSwipeRefreshLayout.setRefreshing(false);

        if (repositoryModelList == null || repositoryModelList.size() <= 0)
            return;

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(new EasyRecyclerAdapter<>(
                this,
                RepositoryViewHolder.class,
                repositoryModelList,
                this));

        recyclerView.enableLoadmore();
    }

    @Override
    public void onRepositoryClicked(Repository repositoryModel) {
        startActivity(PullRequestActivity.newInstance(this, repositoryModel));
    }

    private void initInjector() {
        RepositoryComponent component = DaggerRepositoryComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(getActivityModule())
                .repositoryModule(new RepositoryModule(this))
                .build();
        component.inject(this);
    }

    private void initSwipeRefresh() {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(true);
                presenter.onLoad();
            }
        });
    }

    private void initPresenter() {
        presenter.setView(this);
        presenter.create();
    }

    private void initToolbar() {
        toolbar.setTitle(getString(R.string.repository));
        setSupportActionBar(toolbar);
    }

    private void initRecyclerView() {
        recyclerView.enableLoadmore();
        recyclerView.setOnLoadMoreListener(new UltimateRecyclerView.OnLoadMoreListener() {
            @Override
            public void loadMore(int itemsCount, int maxLastVisiblePosition) {
                recyclerView.disableLoadmore();
                presenter.onLoadMore();
            }
        });
    }
}
