package dev.broto.desafio_android.ui.adapter;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;

import de.hdodenhof.circleimageview.CircleImageView;
import dev.broto.desafio_android.R;
import dev.broto.domain.entity.PullRequest;
import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;

/**
 * Created by broto on 11/19/15.
 */
@LayoutId(R.layout.item_pull_request)
public class PullRequestViewHolder extends ItemViewHolder<PullRequest> {

    @ViewId(R.id.layout_pull_request_body)
    LinearLayout layout;

    @ViewId(R.id.img_pr_owner_photo)
    CircleImageView imgPhoto;

    @ViewId(R.id.lbl_pr_title)
    TextView lblTitle;

    @ViewId(R.id.lbl_pr_content)
    TextView lblContent;

    @ViewId(R.id.lbl_pr_owner_name)
    TextView lblOwnerName;

    @ViewId(R.id.lbl_pr_date)
    TextView lblDate;

    public PullRequestViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(PullRequest item, PositionInfo positionInfo) {
        Picasso.with(getContext()).load(item.getUser().getAvatarUrl()).into(imgPhoto);

        lblTitle.setText(item.getTitle());
        lblContent.setText(item.getBody());
        lblOwnerName.setText(String.valueOf(item.getUser().getLogin()));

        DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(getContext());
        lblDate.setText(dateFormat.format(item.getCreatedAt()));
    }

    @Override
    public void onSetListeners() {
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PullRequestHolderListener listener = getListener(PullRequestHolderListener.class);
                if (listener != null) {
                    listener.onPullRequestClicked(getItem());
                }
            }
        });
    }

    public interface PullRequestHolderListener {
        void onPullRequestClicked(PullRequest pullRequest);
    }
}
