package dev.broto.desafio_android.ui.adapter;

import android.support.v7.widget.CardView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import dev.broto.desafio_android.R;
import dev.broto.domain.entity.Repository;
import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;

/**
 * Created by broto on 11/19/15.
 */
@LayoutId(R.layout.item_repository)
public class RepositoryViewHolder extends ItemViewHolder<Repository> {

    @ViewId(R.id.card_repository_root)
    CardView card;

    @ViewId(R.id.img_repository_photo)
    CircleImageView imgPhoto;

    @ViewId(R.id.lbl_repository_name)
    TextView lblName;

    @ViewId(R.id.lbl_repository_description)
    TextView lblDescription;

    @ViewId(R.id.lbl_stars_count)
    TextView lblStarsCount;

    @ViewId(R.id.lbl_forks_count)
    TextView lblForksCount;

    @ViewId(R.id.lbl_author_name)
    TextView lblAuthorName;

    public RepositoryViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(Repository item, PositionInfo positionInfo) {
        Picasso.with(getContext()).load(item.getOwner().getAvatarUrl()).into(imgPhoto);

        lblName.setText(item.getName());
        lblDescription.setText(item.getDescription());
        lblStarsCount.setText(String.valueOf(item.getStarsCount()));
        lblForksCount.setText(String.valueOf(item.getForksCount()));
        lblAuthorName.setText(item.getOwner().getName());
    }

    @Override
    public void onSetListeners() {
        card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RepositoryHolderListener listener = getListener(RepositoryHolderListener.class);
                if (listener != null) {
                    listener.onRepositoryClicked(getItem());
                }
            }
        });

        imgPhoto.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()){
                    case MotionEvent.ACTION_DOWN:
                        lblAuthorName.setVisibility(View.VISIBLE);
                        break;

                    case MotionEvent.ACTION_UP:
                        lblAuthorName.setVisibility(View.INVISIBLE);
                        break;

                    default:
                        lblAuthorName.setVisibility(View.INVISIBLE);
                        break;
                }

                return true;
            }
        });
    }

    public interface RepositoryHolderListener {
        void onRepositoryClicked(Repository repositoryModel);
    }
}
