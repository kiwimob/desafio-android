package dev.broto.desafio_android.view;

import android.content.Intent;

import java.util.List;

import dev.broto.domain.entity.PullRequest;

/**
 * Created by broto on 11/19/15.
 */
public interface PullRequestView extends View {

    void launchActivity(Intent intent);

    void renderPullRequests(List<PullRequest> pullRequests);
}
