package dev.broto.desafio_android.view;

import java.util.List;

import dev.broto.domain.entity.Repository;

/**
 * Created by broto on 11/19/15.
 */
public interface RepositoryView extends View {

    void renderRepositories(List<Repository> repositoryModelList);
}
