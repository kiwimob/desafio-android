package dev.broto.desafio_android.view;

/**
 * Created by broto on 11/19/15.
 */
public interface View {

    void showLoading();
    void hideLoading();
    void showErrorMessage(String message);

}
